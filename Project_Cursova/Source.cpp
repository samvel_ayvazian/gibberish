#include "Header.h"

BOOL NewWindowClass(WNDPROC Proc, TCHAR szName[], UINT brBackground, UINT icon, UINT cursor, UINT menu)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(wcex);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = Proc;
	wcex.cbClsExtra = wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, (LPCSTR)icon);
	wcex.hCursor = LoadCursor(NULL, IDC_HAND);
	wcex.hbrBackground = (HBRUSH)(BLACK_BRUSH);
	wcex.lpszMenuName = (LPCSTR)menu;
	wcex.lpszClassName = szName;
	wcex.hIconSm = LoadIcon(hInst, (LPCSTR)icon);
	if (!RegisterClassEx(&wcex))
	{
		MessageBox(NULL, "������� ���������������� ����� ����������� ��������.", szName, NULL);
		return 0;
	}
	return 1;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInst = hInstance;
	if (!NewWindowClass(WndProc, szWindowClass, COLOR_WINDOW, IDI_MAIN_ICON, IDC_MAIN_CURSOR, IDR_MAIN_MENU)) 
		return FALSE;
	hWnd = CreateWindow(szWindowClass, szTitle, WS_POPUP | WS_CAPTION | WS_VISIBLE, 
		183, 84, 1000, 600, NULL, NULL, hInst, NULL);
	if (!hWnd)
	{
		MessageBox(NULL, "Call to CreateWindow failed!", "Win32 Guided Tour", NULL);
		return 1;
	}
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(hWnd, hAccel, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	/* �������� ������������*/
	hAccel = LoadAccelerators(hInst, (LPCSTR)IDR_ACCELERATOR1);

	/* ���������� ��� ����������� �������� */
	HBITMAP h_bMusic_On, h_bMusic_Off;
	
	/* ����������, ��������� � ������� � ������ */
	static OPENFILENAME ofn;				// ���������� ��������� ��� ���������� ������
	static char szFile[256];				// ���������� ��� ���������� ��������� ���
											// ������ � ���� "����\0 ����1\0 ..."
	static char szFileTitle[256];			// ��� ���������� ����� ��� ����, 
											// ���� ������ ������ ���� ����
	static char szCustomFilter[256]; 		// ������ ��� ���������� �������� 

	/* ����������, ��������� � ������� � ������ ������ */
	static CHOOSECOLOR cc;					// ���������� ��������� ��� �������
	static COLORREF clf,  					// ���������� ��� ���������� �����
					clfCust[16];			// �������������� �����

	/* ����������, ��������� � ������� �� ������� */
	static CHOOSEFONT cf;   				// ���������� ��������� ��� ������ �������
	static LOGFONT lf;						// ��� ���������� ���������� ���������� ������
	static short bkText;					// ���������� ��� ������ ���� ������
	static char szFontStyle[LF_FACESIZE]; 	// � ���� ������ ������� �������� ������ ��������

	/* ����������� ��� ��������� � �������� �������� ���� */
	HDC hdc, hCompDC;
	PAINTSTRUCT pc;
	RECT rc;
	BITMAP bitmap;
	HANDLE hBitmap, oBitmap;
	GetClientRect(hWnd, &rc);

	/* ���������� ������� ������ � ��������� ����� */
	int left = rc.left + 17, right = rc.right, top = rc.top, bottom = rc.bottom - 100; 

	/* ������ ������ � ����������, ����������� ���� */
	static int mode = 0;

	switch (message)
	{
	case WM_CREATE: 
	{
		/* ���������� ����� ��������� FILE */
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFileTitle = szFileTitle;
		ofn.nMaxFileTitle = sizeof(szFileTitle);
		ofn.lpstrFilter = "������� ����� (*.txt)\0*.txt\0"; 
		ofn.nFilterIndex = 1;
		ofn.lpstrDefExt = "txt";

		/* ���������� ����� ��������� COLOR */
		cc.lStructSize = sizeof(CHOOSECOLOR); 		
		// ���������� �������������� �����
		for (int i = 0; i < 16; i++)				
			clfCust[i] = RGB(rand()%255, rand()%255, rand()%255);
		// ��������� �� �������������� �����
		cc.lpCustColors = clfCust;					
		// ���������� ��� ��������� ����� | ���������� ������ ������ ������ | ��������� ������ "���������� ����"
		cc.Flags = CC_ANYCOLOR | CC_FULLOPEN | CC_PREVENTFULLOPEN;		

		/* ���������� ����� ��������� FONT */
		cf.lStructSize = sizeof(CHOOSEFONT);		
		cf.lpLogFont = &lf;
		// ���������� ������ �������� ������ | ������������ �������� lf ��� ������������� ������
		cf.Flags = CF_SCREENFONTS |	CF_INITTOLOGFONTSTRUCT;	
		// ������ �� ��������� �� �������
		cf.lpszStyle = (LPSTR)szFontStyle;  		
		// ���������� ��� ������
		bkText = TRANSPARENT;						

		/* ��������� ������ */
		PlaySound("music.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);

		/* ��� ������, ��������, ����� ������, ����������, �������*/
		// ���������� � �����...
		hInfo = CreateWindow("Button", "���������� ��� ����...", 
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left, bottom, 200, 40, hWnd, (HMENU)ID_INFO, hInst, NULL);
		// ������ ������...
		hMain_Menu = CreateWindow("Button", "������ ������...", 
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left+380, bottom, 200, 40, hWnd, (HMENU)ID_STARTMENU, hInst, NULL);
		// ��� ������...
		hAbout_Author = CreateWindow("Button", "��� ������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left+750, bottom, 200, 40, hWnd, (HMENU)ID_ABOUT_AUTHOR, hInst, NULL);
		// ������� ����...
		hOpen = CreateWindow("Button", "������� ����...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 370, bottom - 15, 200, 40, hWnd, (HMENU)ID_OPEN, hInst, NULL);
		// �����������...
		hCrypt = CreateWindow("Button", "��(/���)���������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 370, bottom - 100, 200, 40, hWnd, (HMENU)ID_CRYPT, hInst, NULL);
		// ��������� ���� ��� �����
		hEdit_Crypt = CreateWindow("Edit", 0,
			WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL
			| ES_MULTILINE | ES_LEFT | ES_AUTOVSCROLL | ES_WANTRETURN,
			left, bottom - 100, 300, 125, hWnd, (HMENU)ID_EDIT_CRYPT, hInst, NULL);
		// ����� �� ���������
		SetWindowText(hEdit_Crypt, "�������� �����...");
		// ��������� ���� ��� ������
		hEdit_Decrypt = CreateWindow("Edit", 0,
			WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL
			| ES_MULTILINE | ES_LEFT | ES_AUTOVSCROLL | ES_READONLY | ES_WANTRETURN,
			left + 645, bottom - 100, 300, 125, hWnd, (HMENU)ID_EDIT_DECRYPT, hInst, NULL);
		// ����� �� ���������
		SetWindowText(hEdit_Decrypt, "���������...");
		// ��������� ���� � ������� ���������� ����...
		hSave_1 = CreateWindow("Button", "��������� ����...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 45, bottom+35, 200, 40, hWnd, (HMENU)ID_SAVE_1, hInst, NULL);
		// ��������� ���� �� ������� ���������� ����...
		hSave_2 = CreateWindow("Button", "��������� ����...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 700, bottom+35, 200, 40, hWnd, (HMENU)ID_SAVE_2, hInst, NULL);
		// �������� ���������� ������� ���������� ����...
		hClear_1 = CreateWindow("Button", "�������� ����������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 45, bottom - 150, 200, 40, hWnd, (HMENU)ID_CLEAR_1, hInst, NULL);
		// �������� ���������� ������� ���������� ����...
		hClear_2 = CreateWindow("Button", "�������� ����������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 700, bottom - 150, 200, 40, hWnd, (HMENU)ID_CLEAR_2, hInst, NULL);
		// ���������� ������.
		hMusicOff = CreateWindow("Button", "����", 
			WS_CHILD | WS_VISIBLE | BS_BITMAP | BS_DEFPUSHBUTTON,
			rc.right-45, 0, 45, 45, hWnd, (HMENU)ID_MUSIC_STOP, hInst, NULL);
		// ��������� ������.
		hMusicOn = CreateWindow("Button", "���",
			WS_CHILD | WS_VISIBLE | BS_BITMAP | BS_DEFPUSHBUTTON,
			rc.right - 45, 0, 45, 45, hWnd, (HMENU)ID_MUSIC_PLAY, hInst, NULL);
		// �������, ������� � �������� ������������� ����
		hRef_Next = CreateWindow("Button", "�����������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 680, bottom + 25, 150, 60, hWnd, (HMENU)ID_REFERENCE_NEXT, hInst, NULL);
		// �������, ������� � �������� ������� ������
		hRef_Accel = CreateWindow("Button", "������� �������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 680, bottom + 25, 150, 60, hWnd, (HMENU)ID_REFERENCE_ACCEL, hInst, NULL);
		// �������, ��������� � ������ �������
		hRef_Return = CreateWindow("Button", "������ �������...",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			left + 680, bottom + 25, 150, 60, hWnd, (HMENU)ID_REFERENCE, hInst, NULL);
		// �������� �������� �� ������ � �������
		h_bMusic_On = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP2));
		h_bMusic_Off = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP1));
		SendMessage(hMusicOn, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)h_bMusic_On);
		SendMessage(hMusicOff, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)h_bMusic_Off);
		ShowWindow(hMusicOn, SW_HIDE);
		Hide_Window();
		ShowWindow(hAbout_Author, SW_SHOW);
		ShowWindow(hMain_Menu, SW_SHOW);
		ShowWindow(hInfo, SW_SHOW);
		return 0;
	}
	case WM_MOVE:
	{
		MoveWindow(hWnd, 183, 84, 1000, 600, FALSE);
		InvalidateRect(hWnd, &rc, TRUE);
	}
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &pc);
		HPEN hpen = CreatePen(PS_SOLID, 5, clf);
		HPEN open = (HPEN)SelectObject(hdc, hpen);
		hBitmap = NULL;
		switch (mode)
		{
		case 0:
		{
			lf.lfHeight = 75;
			lf.lfCharSet = RUSSIAN_CHARSET;
			lf.lfPitchAndFamily = FF_ROMAN;
			HFONT hf = CreateFontIndirect(&lf);
			HFONT of = (HFONT)SelectObject(hdc, hf);
			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, clf);
			DrawText(hdc, szWelcome, strlen(szWelcome), &rc, DT_CENTER | TA_BASELINE);
			SelectObject(hdc, of);
			DeleteObject(hf);
			break;
		}
		case 1:
			hBitmap = LoadImage(NULL, "1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);	
			break;
		case 2:
			hBitmap = LoadImage(NULL, "2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);	
			break;
		case 3:
			hBitmap = LoadImage(NULL, "3.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			break;
		case 4:
			hBitmap = LoadImage(NULL, "4.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			break;
		case 5:
			hBitmap = LoadImage(NULL, "5.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			break;
		case 6:
			hBitmap = LoadImage(NULL, "6.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			break;
		case 7:
			hBitmap = LoadImage(NULL, "7.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			break;
		}
		GetObject(hBitmap, sizeof(BITMAP), &bitmap);
		GetClientRect(hWnd, &rc);
		hCompDC = CreateCompatibleDC(hdc);
		oBitmap = SelectObject(hCompDC, hBitmap);
		StretchBlt(hdc, rc.left, rc.top, rc.right, rc.bottom,
			hCompDC, 0, 0, bitmap.bmWidth, bitmap.bmHeight,
			SRCCOPY);
		SelectObject(hCompDC, oBitmap);
		DeleteObject(hBitmap);
		DeleteDC(hCompDC);
		EndPaint(hWnd, &pc);
		break;
	}
	case WM_COMMAND:
	{
		char str_1[MAX_TEXT_LENGTH], str_2[MAX_TEXT_LENGTH];
		FILE *fp;
		switch (LOWORD(wParam))
		{
		// ������ �� ��������� ���� ���������
		case ID_MAINMENU:	
			mode = 0;
			Hide_Window();
			ShowWindow(hAbout_Author, SW_SHOW);
			ShowWindow(hMain_Menu, SW_SHOW);
			ShowWindow(hInfo, SW_SHOW);
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// ������ � ������������ ���� ���������
		case ID_STARTMENU:
			mode = 1;
			Hide_Window();
			ShowWindow(hEdit_Crypt, SW_SHOW);
			ShowWindow(hEdit_Decrypt, SW_SHOW);
			ShowWindow(hOpen, SW_SHOW);
			ShowWindow(hSave_1, SW_SHOW);
			ShowWindow(hSave_2, SW_SHOW);
			ShowWindow(hClear_1, SW_SHOW);
			ShowWindow(hClear_2, SW_SHOW);
			ShowWindow(hCrypt, SW_SHOW);
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// ��������� ���������� Editbox_1
		case ID_SAVE_1: 
		{
			ofn.Flags = OFN_OVERWRITEPROMPT;											
			ofn.lpstrTitle = "���������� �����..."; 			
			szFile[0] = '\0'; 								
			szFileTitle[0] = '\0';
			if (GetSaveFileName(&ofn))
			{
				fp = fopen(ofn.lpstrFile, "w+");
				for (int i = 0; i < MAX_TEXT_LENGTH; i++)
					str_1[i] = '\0';
				GetWindowText(hEdit_Crypt, str_1, MAX_TEXT_LENGTH);
				fwrite(str_1, sizeof(char), strlen(str_1), fp);
				fclose(fp);
				strcat_s(str_1, "\0");
			}
			break;
		}
		// ��������� ���������� Editbox_2
		case ID_SAVE_2: 
		{
			ofn.Flags = OFN_OVERWRITEPROMPT;											
			ofn.lpstrTitle = "���������� �����..."; 			
			szFile[0] = '\0'; 								
			szFileTitle[0] = '\0';
			if (GetSaveFileName(&ofn))
			{
				fp = fopen(ofn.lpstrFile, "w+");
				for (int i = 0; i < MAX_TEXT_LENGTH; i++)
					str_2[i] = '\0';
				GetWindowText(hEdit_Decrypt, str_2, MAX_TEXT_LENGTH);
				fwrite(str_2, sizeof(char), strlen(str_2), fp);
				fclose(fp);
				strcat_s(str_2, "\0");
			}
			break;
		}
		// �������� ���������� Editbox_1
		case ID_CLEAR_1:
		{
			for (int i = 0; i < MAX_TEXT_LENGTH; i++)
				str_1[i] = '\0';
			SetWindowText(hEdit_Crypt, str_1);
			break;
		}
		// �������� ���������� Editbox_2
		case ID_CLEAR_2:
		{
			for (int i = 0; i < MAX_TEXT_LENGTH; i++)
				str_2[i] = '\0';
			SetWindowText(hEdit_Decrypt, str_2);
			break;
		}
		// ������� ��������� ����
		case ID_OPEN: 
		{
			ofn.Flags = OFN_EXPLORER;
			ofn.lpstrTitle = "³������� �����...";
			szFile[0] = '\0';
			szFileTitle[0] = '\0';
			if (GetOpenFileName(&ofn))
			{
				fp = fopen(ofn.lpstrFile, "r");
				for (int i = 0; i < MAX_TEXT_LENGTH; i++)
					str_1[i] = '\0';
				fread(str_1, sizeof(char), MAX_TEXT_LENGTH, fp);
				SetWindowText(hEdit_Crypt, str_1);
				fclose(fp);
			}
			break;
		}
		// �����������
		case ID_CRYPT:
		{
			GetWindowText(hEdit_Crypt, str_1, 1024);
			int symbol_count;
			for (int i = 0; i < strlen(str_1); i++)
			{
				symbol_count = 0;
				while (str_1[i] != Alphabet_1[symbol_count])
					symbol_count++;
				if (str_1[i] == '\r' || str_1[i] == '\n')
				{
					str_2[i] = str_1[i];
					continue;
				}
				str_2[i] = Alphabet_2[symbol_count];
			}
			for (int i = strlen(str_1); i < MAX_TEXT_LENGTH; i++)
				str_2[i] = '\0';
			SetWindowText(hEdit_Decrypt, str_2);
			break;
		}
		// ������ � �������� ���������� ����
		case ID_INFO:
			mode = 2;
			Hide_Window();
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// ������ � ��������������� ������
		case ID_FONT:
			if (ChooseFont(&cf))		
				bkText = TRANSPARENT; 	
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_COLOR:
			if (ChooseColor(&cc)) 		
				clf = cc.rgbResult;		
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		// � ���������
		case ID_ABOUT_PROGRAM:
			mode = 3;
			Hide_Window();
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// �� ������
		case ID_ABOUT_AUTHOR:
			mode = 4;
			Hide_Window();
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// �������, ����� 1.
		case ID_REFERENCE:
			mode = 5;
			Hide_Window();
			ShowWindow(hRef_Next, SW_SHOW);
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// �������, ����� 2.
		case ID_REFERENCE_NEXT:
			mode = 6;
			ShowWindow(hRef_Next, SW_HIDE);
			ShowWindow(hRef_Accel, SW_SHOW);
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// �������, ����� 3.
		case ID_REFERENCE_ACCEL:
			mode = 7;
			ShowWindow(hRef_Accel, SW_HIDE);
			ShowWindow(hRef_Return, SW_SHOW);
			InvalidateRect(hWnd, &rc, TRUE);
			break;
		// ����� �� ���������
		case ID_QUIT:
			DestroyWindow(hWnd);
			break;
		// ������ � �������
		case ID_MUSIC_PLAY:
			PlaySound("music.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
			ShowWindow(hMusicOn, SW_HIDE);
			ShowWindow(hMusicOff, SW_SHOW);
			break;
		case ID_MUSIC_STOP:
			PlaySound(NULL, NULL, SND_PURGE);
			ShowWindow(hMusicOff, SW_HIDE);
			ShowWindow(hMusicOn, SW_SHOW);
			break;
		}
		break;
	}
	case WM_DESTROY: { PostQuitMessage(0); return 0; }
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void Hide_Window()
{
	ShowWindow(hEdit_Crypt, SW_HIDE);
	ShowWindow(hEdit_Decrypt, SW_HIDE);
	ShowWindow(hRef_Next, SW_HIDE);
	ShowWindow(hRef_Return, SW_HIDE);
	ShowWindow(hRef_Accel, SW_HIDE);
	ShowWindow(hOpen, SW_HIDE);
	ShowWindow(hSave_1, SW_HIDE);
	ShowWindow(hSave_2, SW_HIDE);
	ShowWindow(hClear_1, SW_HIDE);
	ShowWindow(hClear_2, SW_HIDE);
	ShowWindow(hCrypt, SW_HIDE);
	ShowWindow(hAbout_Author, SW_HIDE);
	ShowWindow(hMain_Menu, SW_HIDE);
	ShowWindow(hInfo, SW_HIDE);
}