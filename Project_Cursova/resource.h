//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Resource.rc
//

/* Основные ID */
#define IDI_MAIN_ICON                   101
#define IDC_MAIN_CURSOR                 102
#define IDR_MAIN_MENU                   103
#define IDB_BITMAP1                     104
#define IDB_BITMAP2                     105
#define IDB_BITMAP3                     106
#define IDR_ACCELERATOR1                107

/* Кнопки */
#define ID_OPEN                         3001
#define ID_MUSIC_PLAY                   3002
#define ID_MUSIC_STOP                   3003
#define ID_SAVE_1                       3004
#define ID_SAVE_2                       3005
#define ID_CRYPT                        3006
#define ID_CLEAR_1                      3007
#define ID_CLEAR_2                      3008
#define ID_EDIT_CRYPT                   3009
#define ID_EDIT_DECRYPT                 3010

/* Кнопки меню */
#define ID_MAINMENU                     40001
#define ID_STARTMENU                    40002
#define ID_INFO                         40003
#define ID_FONT                         40004
#define ID_COLOR                        40005
#define ID_ABOUT_PROGRAM                40006
#define ID_ABOUT_AUTHOR                 40007
#define ID_REFERENCE                    40008
#define ID_REFERENCE_NEXT               40009
#define ID_REFERENCE_ACCEL              40010
#define ID_QUIT                         40011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40043
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
